package com.epam.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.customException.UserNotFoundException;
import com.epam.dto.UserDto;
import com.epam.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
@Api(value = "User Service Rest End Point")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	
	private Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	UserService UserService;

	@ApiOperation(value = "returns all users")
	@ApiResponse(code = 200, message = "Success")
	@GetMapping
	public ResponseEntity<List<UserDto>> findAllUsers() {
		logger.info("get all users");
		return ResponseEntity.ok(UserService.findAll());
	}

	@ApiOperation(value = "Return the user based on user id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 404, message = "User Not Found") })
	@GetMapping("/{UserId}")
	public ResponseEntity<UserDto> findUserById(
			@ApiParam(value = "UserId to get the user details", required = true) @PathVariable(value = "UserId") Long id) {
		ResponseEntity<UserDto> response;
		try {
			logger.info("get all user by id:{}",id);
			response = new ResponseEntity<>(UserService.findById(id), HttpStatus.OK);
		} catch (UserNotFoundException e) {
			logger.error("Exception occured while getting user by id:{}",id);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@ApiOperation(value = "Creates a new user")
	@ApiResponse(code = 200, message = "Success")
	@PostMapping
	public ResponseEntity<UserDto> addNewUser(
			@ApiParam(value = "User object to add as a new user", required = true) @RequestBody UserDto UserDto) {
		logger.info("add a new user");
		return ResponseEntity.ok(UserService.add(UserDto));
	}

	@ApiOperation(value = "Deletes the user by UserId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 404, message = "User Not Found") })
	@DeleteMapping("/{UserId}")
	public ResponseEntity<?> deleteUserById(
			@ApiParam(value = "UserId to delete a user", required = true) @PathVariable(value = "UserId") Long id) {
		ResponseEntity<?> response;
		try {
			logger.info("delete user by id:{}",id);
			UserService.deleteById(id);
			response = new ResponseEntity<>(HttpStatus.OK);
		} catch (UserNotFoundException e) {
			logger.error("Exception occured while deleting user with id : {}",id);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@ApiOperation(value = "Updates the user by userId and user object")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 404, message = "User Not Found") })
	@PutMapping("/{UserId}")
	public ResponseEntity<UserDto> updateUser(
			@ApiParam(value = "UserId to update a user", required = true) @PathVariable(value = "UserId") Long id,
			@ApiParam(value = "User object to update a user", required = true) @RequestBody UserDto UserDto) {
		ResponseEntity<UserDto> response;
		try {
			logger.info("update user with id : {}",id);
			response = new ResponseEntity<>(UserService.update(id, UserDto), HttpStatus.OK);
		} catch (UserNotFoundException e) {
			logger.error("Exception occured while updating user with id:{}",id);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}

}
