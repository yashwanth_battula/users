package com.epam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.customException.UserNotFoundException;
import com.epam.dto.UserDto;
import com.epam.entity.User;
import com.epam.repository.UserRepository;

@Service
public class UserService {

	private Logger logger = LogManager.getLogger(UserService.class);
	
	@Autowired
	UserRepository userRepository;

	public List<UserDto> findAll() {
		logger.info("get all users");
		return userRepository.findAll().stream().map(UserDto::new).collect(Collectors.toList());
	}

	public UserDto findById(Long id) throws UserNotFoundException {
		logger.info("get user by id:{}",id);
		return userRepository.findById(id).map(UserDto::new)
				.orElseThrow(() -> new UserNotFoundException("User not found"));
	}

	public UserDto add(UserDto userDto) {
		logger.info("add new user");
		User user = new User();
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmail(userDto.getEmail());
		User userAdded = userRepository.save(user);
		return new UserDto(userAdded);
	}

	public Boolean deleteById(Long id) throws UserNotFoundException {
		Boolean isDeleted = false;
		if (userRepository.findById(id).isPresent()) {
			logger.info("delete user by id:{}",id);
			userRepository.deleteById(id);
			isDeleted = true;
		} else {
			logger.error("Exception occured while deleting user by id:{}",id);
			throw new UserNotFoundException("User not found with id:" + id);
		}
		return isDeleted;
	}

	public UserDto update(Long id, UserDto userDto) throws UserNotFoundException {
		logger.info("update user by id:{}",id);
		User newUser = new User(id, userDto.getFirstName(), userDto.getLastName(), userDto.getEmail());
		userRepository.findById(id).map(oldUser -> userRepository.save(newUser))
				.orElseThrow(() -> new UserNotFoundException("User not found with id" + id));
		return new UserDto(newUser);
	}
}
