package com.epam.customException;

public class UserNotFoundException extends RuntimeException{

	public UserNotFoundException(String errorMsg){
		super(errorMsg);
	}
}
