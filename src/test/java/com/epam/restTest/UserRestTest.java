package com.epam.restTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.epam.customException.UserNotFoundException;
import com.epam.dto.UserDto;
import com.epam.rest.UserController;
import com.epam.service.UserService;

public class UserRestTest {

	@Mock
	UserService userService;

	@InjectMocks
	UserController userController;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetAllusers() {
		List<UserDto> usersList = new ArrayList<>();
		usersList.add(new UserDto(1L, "ab", "ab", "ab"));
		when(userService.findAll()).thenReturn(usersList);
		ResponseEntity<List<UserDto>> responseEntity = userController.findAllUsers();
		assertThat(responseEntity.getBody().equals(usersList));
	}

	@Test
	void testGetuserById() throws UserNotFoundException {
		UserDto UserDto = new UserDto(1L, "abc", "abc", "abc");
		when(userService.findById(1L)).thenReturn(UserDto);
		ResponseEntity<UserDto> responseEntity = userController.findUserById(1L);
		assertThat(responseEntity.getBody().equals(UserDto));
	}

	@Test
	void testAddNewuser() {
		UserDto UserDto = new UserDto(1L, "abc", "abc", "abc");
		when(userService.add(UserDto)).thenReturn(UserDto);
		ResponseEntity<UserDto> responseEntity = userController.addNewUser(UserDto);
		assertThat(responseEntity.getBody().equals(UserDto));
	}

	@Test
	void testDeleteuser() throws UserNotFoundException {
		when(userService.deleteById(1L)).thenReturn(true);
		ResponseEntity responseEntity = userController.deleteUserById(1L);
		assertThat(responseEntity.getStatusCode().equals(HttpStatus.OK));
	}

	@Test
	void testUpdateuser() throws UserNotFoundException {
		UserDto userDto = new UserDto(1L, "abc", "abc", "abc");
		when(userService.update(1L, userDto)).thenReturn(userDto);
		ResponseEntity<UserDto> responseEntity = userController.updateUser(1L, userDto);
		assertThat(responseEntity.getBody().equals(userDto));
	}

}
