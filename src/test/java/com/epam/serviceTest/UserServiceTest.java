package com.epam.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.customException.UserNotFoundException;
import com.epam.dto.UserDto;
import com.epam.entity.User;
import com.epam.repository.UserRepository;
import com.epam.service.UserService;

public class UserServiceTest {

	@Mock
	UserRepository userRepository;

	@InjectMocks
	UserService userService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAllUsers() {
		List<User> usersList = new ArrayList<>();
		User user = new User(1L, "abc", "abc", "abc");
		usersList.add(user);
		when(userRepository.findAll()).thenReturn(usersList);
		List<UserDto> userDtoList = new ArrayList<>();
		userDtoList.add(new UserDto(user));
		assertEquals(userDtoList.size(), userService.findAll().size());
	}

	@Test
	public void testFindUserById() throws UserNotFoundException {
		User user = new User(1L, "abc", "abc", "abc");
		Optional<User> userOptional = Optional.ofNullable(user);
		when(userRepository.findById(1L)).thenReturn(userOptional);
		UserDto expectedUser = new UserDto(user);
		UserDto actualUser = userService.findById(1L);
		assertEquals(expectedUser.getId(), actualUser.getId());
		assertEquals(expectedUser.getEmail(), actualUser.getEmail());
		assertEquals(expectedUser.getFirstName(), actualUser.getFirstName());
		assertEquals(expectedUser.getLastName(), actualUser.getLastName());
	}

	@Test
	public void testIfNullInFindUserById() {
		when(userRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(UserNotFoundException.class, () -> userService.findById(1L));
	}

//	@Test
//	public void testAddNewUser() {
//		UserDto expectedUser = new UserDto(1L, "abc", "abc", "abc");
//		UserDto actualUser = userService.add(expectedUser);
//		assertEquals(expectedUser.getEmail(), actualUser.getEmail());
//		assertEquals(expectedUser.getFirstName(), actualUser.getFirstName());
//		assertEquals(expectedUser.getLastName(), actualUser.getLastName());
//	}

	@Test
	public void testDeleteUserById() throws UserNotFoundException {
		User user = new User(1L, "abc", "abc", "abc");
		Optional<User> userOptional = Optional.ofNullable(user);
		when(userRepository.findById(1L)).thenReturn(userOptional);
		assertEquals(true, userService.deleteById(1L));
	}

	@Test
	public void testIfNullInDeleteUser() {
		when(userRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(UserNotFoundException.class, () -> userService.deleteById(1L));
	}

//	@Test
//	public void testUpdateUser() throws UserNotFoundException {
//		User user = new User(1L, "abc", "abc", "abc");
//		Optional<User> userOptional = Optional.ofNullable(user);
//		when(userRepository.findById(1L)).thenReturn(userOptional);
//		UserDto expectedUser = new UserDto(1L, "abcd", "abcd", "abcd");
//		UserDto actualUser = userService.update(1L, expectedUser);
//		assertEquals(expectedUser.getEmail(), actualUser.getEmail());
//		assertEquals(expectedUser.getFirstName(), actualUser.getFirstName());
//		assertEquals(expectedUser.getLastName(), actualUser.getLastName());
//	}

	@Test
	public void testIfNullInUpdateUser() {
		when(userRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(UserNotFoundException.class,
				() -> userService.update(1L, new UserDto(1L, "abcd", "abcd", "abcd")));
	}
}
